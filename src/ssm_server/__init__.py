import subprocess
import pathlib
import os
import sys
import importlib.util


def run():
    if len(sys.argv) < 2:
        raise Exception("File argument is missing")

    if not sys.argv[1].lower().endswith(".py"):
        raise Exception("File is not a Python script")

    if not os.path.isfile(sys.argv[1]):
        raise Exception("File '%s' does not exist in current folder" % sys.argv[1])

    path = pathlib.Path(__file__).parent.resolve()
    path = os.path.join(path, "core.py")

    os.environ["MODULE_FILE"] = sys.argv[1]

    try:
        subprocess.run(["fastapi", "dev", path])
    except KeyboardInterrupt:
        pass
