import sys
import os.path
import importlib.util
from typing import Union, Dict

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import RedirectResponse
from pydantic import BaseModel


# import a user-provided space system model file
module_file = os.getenv("MODULE_FILE")
module_name, _ = os.path.splitext(module_file)
module_file_path = os.path.abspath(module_file)
module_path = os.path.dirname(module_file_path)

sys.path.append(module_path)

spec = importlib.util.spec_from_file_location(module_name, module_file_path)
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)


# create fastapi server
app = FastAPI()


@app.get("/")
async def home():
    return RedirectResponse(url="/docs")


@app.websocket("/websocket")
async def connect(websocket: WebSocket):
    await websocket.accept()
    module.connected_clients.add(websocket)
    try:
        while True:
            message = await websocket.receive_text()
    except WebSocketDisconnect:
        module.connected_clients.add(websocket)


class ActivityBody(BaseModel):
    path: str
    arguments: Union[Dict, None] = None
    directives: Union[Dict, None] = None


@app.post("/activities")
async def activity(body: ActivityBody):
    if body.path not in module.activities:
        return {"result": "undefined"}

    func = module.activities[body.path]
    try:
        result = await func(body.arguments, body.directives)
    except Exception:
        return {"result": "aborted"}

    if result in [True, None]:
        return {"result": "confirmed"}
    if result is False:
        return {"result": "unconfirmed"}
    return {"result": "undefined"}


class EventBody(BaseModel):
    path: str


@app.post("/events")
async def event(body: EventBody):
    if body.path not in module.events:
        return {"result": "undefined"}

    func = module.events[body.path]
    try:
        result = await func()
    except Exception:
        return {"result": "undefined"}

    if result in [True, False]:
        return {"result": result}


class ReportingDataBody(BaseModel):
    path: str


@app.post("/reporting_data")
async def reporting_data(body: EventBody):
    if body.path not in module.reporting_data:
        return {"result": "undefined"}

    func = module.reporting_data[body.path]
    try:
        result = await func()
    except Exception:
        return {"result": "undefined"}

    return {"result": result}
