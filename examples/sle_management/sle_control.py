import sle


class SleControl:

    def __init__(self):
        self._connections = {}

    def bind(self, arguments):
        if "service_instance" not in arguments:
            raise ValueError
        if arguments["service_instance"] in self._connections:
            raise ValueError
        service = sle.RafServiceUser(
            service_instance_identifier=arguments["service_instance"],
            responder_host="localhost",
            responder_port=55529,
            auth_level=None,
            local_identifier="SLE_USER",
            peer_identifier="SLE_PROVIDER",
            local_password="",
            peer_password="",
        )
        self._connections[arguments["service_instance"]] = service
        service.bind()
