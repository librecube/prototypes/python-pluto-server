import json
from functools import partial


activities = {}
reporting_data = {}
events = {}
connected_clients = set()


async def dispatch(action, arguments, directives):
    speed = arguments.get("speed") if arguments else None
    movement = {"action": action, "value": speed}
    for client in connected_clients:
        await client.send_text(json.dumps(movement))


activities["Rover/Wheel_Drive/drive_forward"] = partial(dispatch, "forward")
activities["Rover/Wheel_Drive/drive_backward"] = partial(dispatch, "backward")
activities["Rover/Wheel_Drive/turn_left"] = partial(dispatch, "left")
activities["Rover/Wheel_Drive/turn_right"] = partial(dispatch, "right")
activities["Rover/Wheel_Drive/stop"] = partial(dispatch, "stop")
