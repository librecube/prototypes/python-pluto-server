import json
from functools import partial
import socket
import time


activities = {}
reporting_data = {}
events = {}
connected_clients = set()


socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
DEST = ("192.168.4.1", 5555)


async def dispatch(action, arguments, directives):
    speed = arguments.get("speed") if arguments else None

    pdu = ("80,17,1" + "\n").encode()
    socket.sendto(pdu, DEST)
    time.sleep(0.1)

    if action == "stop":
        pdu = ("80,8,1,1" + "\n").encode()
        socket.sendto(pdu, DEST)
        time.sleep(0.1)

    if action == "forward":
        pdu = ("80,8,1,6" + "\n").encode()
        socket.sendto(pdu, DEST)
        pdu = (f"80,8,1,2,{speed}" + "\n").encode()
        socket.sendto(pdu, DEST)
        time.sleep(0.1)
    if action == "backward":
        pdu = ("80,8,1,6" + "\n").encode()
        socket.sendto(pdu, DEST)
        pdu = (f"80,8,1,3,{speed}" + "\n").encode()
        socket.sendto(pdu, DEST)
        time.sleep(0.1)
    if action == "left":
        pdu = (f"80,8,1,4,{speed}" + "\n").encode()
        socket.sendto(pdu, DEST)
        time.sleep(0.1)
    if action == "right":
        pdu = (f"80,8,1,5,{speed}" + "\n").encode()
        socket.sendto(pdu, DEST)
        time.sleep(0.1)

    # for clients
    movement = {"action": action, "value": speed}
    for client in connected_clients:
        await client.send_text(json.dumps(movement))


activities["Rover/Wheel_Drive/drive_forward"] = partial(dispatch, "forward")
activities["Rover/Wheel_Drive/drive_backward"] = partial(dispatch, "backward")
activities["Rover/Wheel_Drive/turn_left"] = partial(dispatch, "left")
activities["Rover/Wheel_Drive/turn_right"] = partial(dispatch, "right")
activities["Rover/Wheel_Drive/stop"] = partial(dispatch, "stop")
