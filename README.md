# Space System Model Server

The interface server for running PLUTO scripts.

## Getting Started

Install the module:

```bash
git clone https://gitlab.com/librecube/prototypes/python-pluto-server
cd python-pluto-server
python -m venv venv
source venv/bin/activate
pip install -e .
```

Start the server:

```bash
cd examples
server
```

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-pluto-server/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-pluto-server

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
